import 'package:flutter/material.dart';
import 'package:flutter_navigation/column_row.dart';
import 'package:flutter_navigation/route.dart';
import 'package:flutter_navigation/styling.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        // '/': (context) => Styling(),
        '/': (context) => Column_row(),
        // '/': (context) => HomePage(),
        // '/about': (context) => AboutPage(),
      },
    );
  }
}
