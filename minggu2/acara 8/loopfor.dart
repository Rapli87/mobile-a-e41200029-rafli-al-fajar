import 'dart:io';

void main(List<String> args) {
  for (int i = 0; i < 20; i++) {
    if (i % 2 == 1) {
      stdout.writeln("$i - Santai");
    }
    if (i % 2 == 0) {
      stdout.writeln("$i - Berkualitas");
    }
    if (i % 3 == 0) {
      stdout.writeln("$i - I Love Coding");
    }
  }
}
