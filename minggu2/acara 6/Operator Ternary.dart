// void main() {
//   var isThisWahyu = true;

//   if (isThisWahyu) {
//     print("wahyu");
//   } else {
//     print("bukan");
//   }
// }

// Nah dengan ternary operator, kita dapat mempersingkatnya menjadi.
void main() {
  var isThisWahyu = true;
  isThisWahyu ? print("wahyu") : print("bukan");
}
